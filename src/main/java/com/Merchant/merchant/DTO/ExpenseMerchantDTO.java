package com.Merchant.merchant.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ExpenseMerchantDTO
{
    private long expense_id;
    private String expense_type;
    private String user_name;
    private long merchant_id;
    private long currency_id;
    private long amount;

}
