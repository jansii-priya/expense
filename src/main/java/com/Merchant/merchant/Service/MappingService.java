package com.Merchant.merchant.Service;

import com.Merchant.merchant.DTO.ExpenseMerchantDTO;
import com.Merchant.merchant.exception.ResourceNotFoundException;
import com.Merchant.merchant.model.Expense;
import com.Merchant.merchant.repository.ExpenseRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MappingService {
    @Autowired
    private ExpenseRepository expenseRepository;
    @Autowired
    private ModelMapper modelMapper;

    public List<ExpenseMerchantDTO> getAllExpensesMerchant(){
        return((List<Expense>)expenseRepository
                .findAll())
                .stream()
                .map(this::convertEntityIntoDTO)
                .collect(Collectors.toList());
    }
    public Expense updateExpense(long expense_id, Expense postRequest) {
        Expense expense = expenseRepository.findById(expense_id)
                .orElseThrow(() -> new ResourceNotFoundException("Expense does not exist" + expense_id));
        expense.setExpense_type(postRequest.getExpense_type());
        expense.setAmount(postRequest.getAmount());
        expense.setUser_name(postRequest.getUser_name());
        expense.setMerchant(postRequest.getMerchant());

        return expenseRepository.save(expense);
    }
    public Expense createExpense(Expense expense) {
        return expenseRepository.save(expense);
    }

    public void deleteExpense(long expense_id) {
        Expense expense = expenseRepository.findById(expense_id)
                .orElseThrow(() -> new ResourceNotFoundException("Expense does not exist "+  expense_id));
        expenseRepository.delete(expense);
    }


    private ExpenseMerchantDTO convertEntityIntoDTO (Expense expense){
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        ExpenseMerchantDTO expenseMerchantDTO = new ExpenseMerchantDTO();
        expenseMerchantDTO = modelMapper.map(expense, ExpenseMerchantDTO.class);

        return expenseMerchantDTO;
    }

    private Expense convertDTOIntoEntity(ExpenseMerchantDTO expenseMerchantDTO){
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        Expense expense = new Expense();
        expense = modelMapper.map(expenseMerchantDTO, Expense.class);
        return expense;
    }
//       private ExpenseMerchantDTO convertDataIntoDTO (Expense expense){
//        ExpenseMerchantDTO dto = new ExpenseMerchantDTO();
//
//        dto.setExpense_id(expense.getExpense_id());
//        dto.setExpense_type(expense.getExpense_type());
//        dto.setUser_name(expense.getUser_name());
//        dto.setAmount(expense.getAmount());
//
//        Merchant merchant = expense.getMerchant();
//
//        dto.setMerchant_id(merchant.getMerchant_id());
//        return dto;
//    }

}
