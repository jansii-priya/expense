package com.Merchant.merchant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "merchant")
public class Merchant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long merchant_id;

    @Column(name = "merchant_name")
    private String merchant_name;
    @Column(name = "merchant_code")
    private String merchant_code;

    @JsonIgnore
    @OneToMany(mappedBy = "merchant", cascade = CascadeType.ALL,orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Expense> expenses;
}
