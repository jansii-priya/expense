package com.Merchant.merchant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "currency")
public class Currency {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long currency_id;
    @Column(name = "currency_code")
    private String currency_code;
    @Column(name = "currency_symbol")
    private String currency_symbol;
    @Column(name = "decimal_place")
    private int decimal_place;

    @JsonIgnore
    @OneToMany(mappedBy = "currency",cascade = CascadeType.ALL,orphanRemoval = true,fetch = FetchType.LAZY)
    private Set<Expense> expenses;
}
