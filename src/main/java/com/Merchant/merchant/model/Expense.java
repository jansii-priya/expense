package com.Merchant.merchant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "expense")
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long expense_id;

    @Column(name = "expense_type")
    private String expense_type;
    @Column(name = "user_name")
    private String user_name;
    @Column(name = "amount")
    private long amount;

    @JsonIgnore
    @ManyToOne(fetch=FetchType.EAGER,optional=false)
    @JoinColumn(name = "merchant_id")
    private Merchant merchant;

    public void expense(Merchant merchant) {
        this.merchant = merchant;
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "currency_id")
    private Currency currency;
    public void expense(Currency currency){
        this.currency = currency;
    }


}
