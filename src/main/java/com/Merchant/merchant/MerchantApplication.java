package com.Merchant.merchant;

import com.Merchant.merchant.model.Currency;
import com.Merchant.merchant.model.Expense;
import com.Merchant.merchant.model.Merchant;
import com.Merchant.merchant.repository.CurrencyRepository;
import com.Merchant.merchant.repository.ExpenseRepository;
import com.Merchant.merchant.repository.MerchantRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MerchantApplication implements CommandLineRunner {

	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}
	public static void main(String[] args) {
		SpringApplication.run(MerchantApplication.class, args);
	}

	@Autowired
	private MerchantRepository merchantRepository;
	private ExpenseRepository expenseRepository;
	private CurrencyRepository currencyRepository;

	@Override
	public void run(String... args) throws Exception {
		/*Merchant merchant = new Merchant();
		merchant.setMerchant_code("MER-003");
		merchant.setMerchant_name("John");
		merchantRepository.save(merchant);

		Merchant merchant1 = new Merchant();
		merchant1.setMerchant_code("MER-004");
		merchant1.setMerchant_name("Steve");
		merchantRepository.save(merchant1);

		Currency currency = new Currency();
		currency.setCurrency_code("AED");
		currency.setCurrency_symbol("AED");
		currency.setDecimal_place(3);
		currencyRepository.save(currency);

		Currency currency1 = new Currency();
		currency1.setCurrency_code("CAD");
		currency1.setCurrency_symbol("$");
		currency1.setDecimal_place(2);
		currencyRepository.save(currency1);

		Expense expense = new Expense();
		expense.setExpense_type("Food ");
		expense.setUser_name("Mr. A");
		expense.setAmount((long) 8000.00);
		expense.setMerchant(merchant1);
		expense.setCurrency(currency1);
		expenseRepository.save(expense);*/


	}

}
