package com.Merchant.merchant.repository;


import com.Merchant.merchant.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Long> {

    //crud database methods
//    Merchant save (Merchant merchant);
}
