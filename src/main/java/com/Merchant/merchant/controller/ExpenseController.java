package com.Merchant.merchant.controller;

import com.Merchant.merchant.exception.ResourceNotFoundException;
import com.Merchant.merchant.model.Expense;
import com.Merchant.merchant.model.Merchant;
import com.Merchant.merchant.repository.ExpenseRepository;
import com.Merchant.merchant.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/expenses")
public class ExpenseController {

    @Autowired
    private ExpenseRepository expenseRepository;
    @Autowired
    private MerchantRepository merchantRepository;

    @GetMapping
    private List<Expense> getAllExpenses(){
        return expenseRepository.findAll();
    }

    @GetMapping("{expense_id}")
    public ResponseEntity<Expense> getExpenseById(@PathVariable long expense_id){
        Expense expense = expenseRepository.findById(expense_id)
                .orElseThrow(() -> new ResourceNotFoundException(("merchant does not exist " + expense_id)));
        return ResponseEntity.ok(expense);
    }
    @PostMapping
    public Expense createExpense (@RequestBody Expense expense){
        return expenseRepository.save(expense);
    }

    @PutMapping("{expense_id}")
    public ResponseEntity<Expense> updateExpense(@PathVariable long expense_id, @RequestBody Expense ExpenseDetails){
        System.out.println(ExpenseDetails.toString());
        Expense updateExpense= expenseRepository.findById(expense_id)
                .orElseThrow(() -> new ResourceNotFoundException("Expense does not exist" + expense_id ));
        updateExpense.setExpense_type(ExpenseDetails.getExpense_type());
        updateExpense.setUser_name(ExpenseDetails.getUser_name());
        updateExpense.setAmount(ExpenseDetails.getAmount());
        updateExpense.setMerchant(ExpenseDetails.getMerchant());
        System.out.println(ExpenseDetails.getMerchant());
        System.out.println(ExpenseDetails.getAmount());
        expenseRepository.save(updateExpense);
        return ResponseEntity.ok(updateExpense);
    }
    @DeleteMapping("{expense_id}")
    public ResponseEntity<HttpStatus> deleteExpense(@PathVariable long expense_id){
        Expense expense = expenseRepository.findById(expense_id)
                .orElseThrow(() -> new ResourceNotFoundException("Expense does not exist " + expense_id));
        expenseRepository.delete(expense);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
//    @PutMapping("{expense_id}")
//    Expense expense(@PathVariable Long expense_id, @PathVariable Long merchant_id){
//        Expense expenses = expenseRepository.findById(expense_id).get();
//        System.out.println(expenses);
//        Merchant merchant = merchantRepository.findById(merchant_id).get();
//        expenses.expense(merchant);
//        System.out.println(expenses);
//        return expenseRepository.save(expenses);
//    }

}
