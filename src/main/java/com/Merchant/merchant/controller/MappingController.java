package com.Merchant.merchant.controller;

import com.Merchant.merchant.DTO.ExpenseMerchantDTO;
import com.Merchant.merchant.Service.MappingService;
import com.Merchant.merchant.model.Expense;
import com.Merchant.merchant.repository.ExpenseRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
@RequestMapping("/api/v1/map")
public class MappingController {
    @Autowired
    private MappingService mappingService;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ExpenseRepository expenseRepository;

    @GetMapping
    public List<ExpenseMerchantDTO> getAllExpensesMerchant(){
        List<ExpenseMerchantDTO> ExpenseMerchant = mappingService.getAllExpensesMerchant();
        return ExpenseMerchant;
    }
    @PutMapping("/{expense_id}")
    public ResponseEntity<ExpenseMerchantDTO> updateExpense(@PathVariable long expense_id,
                                                            @RequestBody ExpenseMerchantDTO expenseMerchantDTO) {
        // convert DTO to Entity
        Expense putRequest = modelMapper.map(expenseMerchantDTO, Expense.class);
        Expense expense = mappingService.updateExpense(expense_id, putRequest);
        // entity to DTO
        ExpenseMerchantDTO putResponse = modelMapper.map(expense, ExpenseMerchantDTO.class);
        return ResponseEntity.ok().body(putResponse);
    }
    @PostMapping
    public ResponseEntity<ExpenseMerchantDTO> createExpense(@RequestBody ExpenseMerchantDTO expenseMerchantDTO) {
        // convert DTO to entity
        Expense postRequest = modelMapper.map(expenseMerchantDTO, Expense.class);
        Expense expense = mappingService.createExpense(postRequest);
        // convert entity to DTO
        ExpenseMerchantDTO postResponse = modelMapper.map(expense, ExpenseMerchantDTO.class);
        return new ResponseEntity<ExpenseMerchantDTO>(postResponse, HttpStatus.CREATED);
    }

    @DeleteMapping("/{expense_id}")
    public ResponseEntity<HttpStatus> deletePost(@PathVariable Long expense_id) {
        mappingService.deleteExpense(expense_id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
