package com.Merchant.merchant.controller;

import com.Merchant.merchant.exception.ResourceNotFoundException;
import com.Merchant.merchant.model.Merchant;
import com.Merchant.merchant.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/merchant")
public class MerchantController {

    @Autowired
    private MerchantRepository merchantRepository;

    @GetMapping
    public List<Merchant> getAllMerchants(){
        return merchantRepository.findAll();
    }

    @PostMapping
    public Merchant createMerchant (@RequestBody Merchant merchant){
        return merchantRepository.save(merchant);
    }

    @GetMapping("{merchant_id}")
    public ResponseEntity<Merchant> getMerchantById(@PathVariable long merchant_id){
        Merchant merchant = merchantRepository.findById(merchant_id)
                .orElseThrow(() -> new ResourceNotFoundException(("merchant does not exist " + merchant_id)));
        return ResponseEntity.ok(merchant);
    }
    @PutMapping("{merchant_id}")
    public ResponseEntity<Merchant> updateMerchant(@PathVariable long merchant_id,@RequestBody Merchant merchantDetails){
        Merchant updateMerchant= merchantRepository.findById(merchant_id)
                .orElseThrow(() -> new ResourceNotFoundException("Merchant does not exist" + merchant_id ));
        updateMerchant.setMerchant_name(merchantDetails.getMerchant_name());
        updateMerchant.setMerchant_code(merchantDetails.getMerchant_code());
        merchantRepository.save(updateMerchant);
        return ResponseEntity.ok(updateMerchant);
    }

    @DeleteMapping("{merchant_id}")
    public ResponseEntity<HttpStatus> deleteMerchant(@PathVariable long merchant_id){
        Merchant merchant = merchantRepository.findById(merchant_id)
                .orElseThrow(() -> new ResourceNotFoundException("Merchant does not exist " + merchant_id));
        merchantRepository.delete(merchant);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
